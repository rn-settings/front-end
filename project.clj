(defproject front-end "0.1.0-SNAPSHOT"

  :plugins [[lein-cljsbuild "1.1.7"]
	    [lein-tools-deps "0.4.5"]
	    [lein-pprint "1.3.2"]
            [lein-shell "0.5.0"]]

  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj" "src/cljs"]

  :lein-tools-deps/config {:config-files [:install :user :project]}

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :shell {:commands {"open" {:windows ["cmd" "/c" "start"]
                             :macosx  "open"
                             :linux   "xdg-open"}}}

  :cljsbuild {
    :builds [{
      :source-paths ["src/cljs"]
      :compiler {
        :output-to "out"
        :optimizations :none
        :pretty-print true}}]}

  :aliases {"dev"          ["with-profile" "dev" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "watch" "app"]]
            "prod"         ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "release" "app"]]
            "build-report" ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]
                            ["shell" "open" "target/build-report.html"]]
            "karma"        ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "compile" "karma-test"]
                            ["shell" "karma" "start" "--single-run" "--reporters" "junit,dots"]]}

  :profiles
  {:dev
   {:dependencies [[binaryage/devtools "1.0.0"]]
    :source-paths ["dev"]}

   :prod { }}

  :prep-tasks [])
